package br.com.itau.rich.produtomicroservice.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.rich.produtomicroservice.models.Produto;
import br.com.itau.rich.produtomicroservice.repositories.ProdutoRepository;

@RestController
public class ProdutoController {

	@Autowired
	private ProdutoRepository produtoRepository;

	// Metodo para consultar todos produtos
	@RequestMapping(method = RequestMethod.GET, path = "/produtos")
	public ResponseEntity<Iterable<Produto>> getAll() {
		return ResponseEntity.ok(produtoRepository.findAll());
	}
	
	// Metodo para consultar 1 produto especifico
	@RequestMapping(method = RequestMethod.GET, path = "/produto/{id}")
	public ResponseEntity<?> buscarProduto(@PathVariable long id) {
		Optional<Produto> optionalProduto = produtoRepository.findById(id);

		if (!optionalProduto.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(optionalProduto.get());
	}

	// Metodo para incluir 1 produto
	@RequestMapping(method = RequestMethod.POST, path = "/produto")
	public Produto criarProduto(@Valid @RequestBody Produto produto) {
		return produtoRepository.save(produto);
	}

	// Metodo para incluir um vetor de produtos
	@RequestMapping(method = RequestMethod.POST, path = "/produtos")
	public Produto[] criarProdutos(@RequestBody Produto produto[]) {
		for (int j = 0; j < produto.length; j++) {
			produtoRepository.save(produto[j]);
		}
		return produto;
	}
}
