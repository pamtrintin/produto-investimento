package br.com.itau.rich.produtomicroservice.models;

public enum TipoProduto {
	RENDA_FIXA,
	RENDA_VARIAVEL,
	POUPANCA
}
