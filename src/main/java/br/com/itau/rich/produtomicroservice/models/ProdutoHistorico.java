package br.com.itau.rich.produtomicroservice.models;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

@Entity
@Table(name="produto_historico")
public class ProdutoHistorico {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "produto_id", nullable = false) 
    @OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	private Produto produto;
	
	@JsonSerialize(using=LocalDateSerializer.class)
	@JsonDeserialize(using=LocalDateDeserializer.class)
	private LocalDate dataRentabilidade;
	
	private Double rentabilidadeMes;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public LocalDate getDataRentabilidade() {
		return dataRentabilidade;
	}

	public void setDataRentabilidade(LocalDate dataRentabilidade) {
		this.dataRentabilidade = dataRentabilidade;
	}

	public Double getRentabilidadeMes() {
		return rentabilidadeMes;
	}

	public void setRentabilidadeMes(Double rentabilidadeMes) {
		this.rentabilidadeMes = rentabilidadeMes;
	}
}
