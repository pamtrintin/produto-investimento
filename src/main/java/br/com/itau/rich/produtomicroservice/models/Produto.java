package br.com.itau.rich.produtomicroservice.models;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

@Entity
@Table(name = "produto")
public class Produto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	// @JsonSerialize(using=UUIDSerializer.class)
	// @JsonDeserialize(using=UUIDDeserializer.class)
	// private UUID uniqueId;

	private TipoProduto tipo;

	private String titulo;

	private String descricao;

	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate dataRentabilidade;

	private Double rentabilidadeDia;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "produto")
	private Set<ProdutoHistorico> produtoHistorico = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoProduto getTipo() {
		return tipo;
	}

	public void setTipo(TipoProduto tipo) {
		this.tipo = tipo;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public LocalDate getDataRentabilidade() {
		return dataRentabilidade;
	}

	public void setDataRentabilidade(LocalDate dataRentabilidade) {
		this.dataRentabilidade = dataRentabilidade;
	}

	public Double getRentabilidadeDia() {
		return rentabilidadeDia;
	}

	public void setRentabilidadeDia(Double rentabilidadeDia) {
		this.rentabilidadeDia = rentabilidadeDia;
	}

}
