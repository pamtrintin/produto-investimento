package br.com.itau.rich.produtomicroservice.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.rich.produtomicroservice.exceptions.ResourceNotFoundException;
import br.com.itau.rich.produtomicroservice.models.ProdutoHistorico;
import br.com.itau.rich.produtomicroservice.repositories.ProdutoHistoricoRepository;
import br.com.itau.rich.produtomicroservice.repositories.ProdutoRepository;

@RestController
public class ProdutoHistoricoController {

	@Autowired
	ProdutoHistoricoRepository produtoHistoricoRepository;

	@Autowired
	ProdutoRepository produtoRepository;

	// Metodo para consultar o historico de 1 produto
    @RequestMapping(method = RequestMethod.GET, path = "/produto/{produtoId}/historico")
    public Page<ProdutoHistorico> consultaHistorico(@PathVariable (value = "produtoId") Long produtoId,
                                                Pageable pageable) {
        return produtoHistoricoRepository.findByProdutoId(produtoId, pageable);
    }
    
	// Metodo para incluir 1 produto
	@RequestMapping(method = RequestMethod.POST, path = "/produto/{produtoId}/historico")
	public @Valid ProdutoHistorico criarProdutoHistorico(@PathVariable(value = "produtoId") Long produtoId,
												@Valid @RequestBody ProdutoHistorico produtoHistorico) {
		
		return produtoRepository.findById(produtoId).map(post -> {
            produtoHistorico.setProduto(post);
            return produtoHistoricoRepository.save(produtoHistorico);
		}).orElseThrow(() -> new ResourceNotFoundException("PostId " + produtoId + " not found"));
	}

	// Metodo para incluir um vetor de produtos
	@RequestMapping(method = RequestMethod.POST, path = "/produtos/historico")
	public ProdutoHistorico[] criarProdutosHistorico(@RequestBody ProdutoHistorico produtoHistorico[]) {
		for (int j = 0; j < produtoHistorico.length; j++) {
			produtoHistoricoRepository.save(produtoHistorico[j]);
		}
		return produtoHistorico;
	}
}
