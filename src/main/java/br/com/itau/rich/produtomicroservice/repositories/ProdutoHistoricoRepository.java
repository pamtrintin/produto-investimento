package br.com.itau.rich.produtomicroservice.repositories;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.itau.rich.produtomicroservice.models.Produto;
import br.com.itau.rich.produtomicroservice.models.ProdutoHistorico;

@Repository
public interface ProdutoHistoricoRepository extends CrudRepository<ProdutoHistorico, Long> {
	@Query("select a from ProdutoHistorico a where a.dataRentabilidade between ?1 and ?2")
	List<Produto> findByDataRentabilidadeInterval(LocalDate dataInicial, LocalDate dataFinal);

	Page<ProdutoHistorico> findByProdutoId(Long produtoId, Pageable pageable);
}
