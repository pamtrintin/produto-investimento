package br.com.itau.rich.produtomicroservice.repositories;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.itau.rich.produtomicroservice.models.Produto;

@Repository
public interface ProdutoRepository extends CrudRepository<Produto, Long> {
}